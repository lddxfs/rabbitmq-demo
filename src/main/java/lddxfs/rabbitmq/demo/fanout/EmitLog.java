package lddxfs.rabbitmq.demo.fanout;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import lddxfs.rabbitmq.demo.common.Constant;
import lddxfs.rabbitmq.demo.common.LocalConnectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * fanout 消息发送端
 */
public class EmitLog {

    private static Logger logger = LoggerFactory.getLogger(EmitLog.class);

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = LocalConnectFactory.getConnectionFactory();
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.exchangeDeclare(Constant.FANOUT_EXCHANGE_NAME, BuiltinExchangeType.FANOUT);
            String message = getMessage(argv);
            channel.basicPublish(Constant.FANOUT_EXCHANGE_NAME, "", null, message.getBytes("UTF-8"));
            logger.info("发送消息'{}'",message);
        }
    }

    private static String getMessage(String[] strings) {
        if (strings.length < 1)
            return "info: Hello World!";
        return String.join(" ", strings);
    }

}

