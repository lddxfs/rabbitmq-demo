package lddxfs.rabbitmq.demo.fanout;

import com.rabbitmq.client.*;
import lddxfs.rabbitmq.demo.common.Constant;
import lddxfs.rabbitmq.demo.common.LocalConnectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * fanout 消息接收端
 * 可以开启多个接收端每个接收端接收到相同的消息
 */
public class ReceiveLogs {
  private static Logger logger = LoggerFactory.getLogger(ReceiveLogs.class);

  public static void main(String[] argv) throws Exception {
    ConnectionFactory factory = LocalConnectFactory.getConnectionFactory();
    Connection connection = factory.newConnection();
    Channel channel = connection.createChannel();

    channel.exchangeDeclare(Constant.FANOUT_EXCHANGE_NAME, BuiltinExchangeType.FANOUT);
    String queueName = channel.queueDeclare().getQueue();
    channel.queueBind(queueName, Constant.FANOUT_EXCHANGE_NAME, "");

    logger.info("正在接收消息...");

    Consumer consumer = new DefaultConsumer(channel) {
      @Override
      public void handleDelivery(String consumerTag, Envelope envelope,
                                 AMQP.BasicProperties properties, byte[] body) throws IOException {
        String message = new String(body, "UTF-8");
        logger.info("接收到消息 '{}'",message);
      }
    };
    channel.basicConsume(queueName, true, consumer);
  }
}

