package lddxfs.rabbitmq.demo.helloworld;

import com.rabbitmq.client.*;
import lddxfs.rabbitmq.demo.common.Constant;
import lddxfs.rabbitmq.demo.common.LocalConnectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 *  第一个demo 接受端
 */
public class Recv {
    private static Logger logger = LoggerFactory.getLogger(Recv.class);

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = LocalConnectFactory.getConnectionFactory();
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(Constant.HELLOWORLD_QUEUE_NAME, false, false, false, null);
        logger.info(" [*] Waiting for messages. To exit press CTRL+C");

        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                    throws IOException {
                String message = new String(body, "UTF-8");
                logger.info(" [x] Received '" + message + "'");
            }
        };
        channel.basicConsume(Constant.HELLOWORLD_QUEUE_NAME, true, consumer);
//        2018-11-05 07:47:48.646 [main] INFO  lddxfs.rabbitmq.helloworld.Recv -  [*] Waiting for messages. To exit press CTRL+C
//        2018-11-05 07:47:48.646 [pool-1-thread-4] INFO  lddxfs.rabbitmq.helloworld.Recv -  [x] Received 'Hello World!'
//        2018-11-05 07:48:33.660 [pool-1-thread-5] INFO  lddxfs.rabbitmq.helloworld.Recv -  [x] Received 'Hello World!'
//        2018-11-05 08:07:39.471 [pool-1-thread-4] INFO  lddxfs.rabbitmq.helloworld.Recv -  [x] Received '中文123'
    }
}
