package lddxfs.rabbitmq.demo.helloworld;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import lddxfs.rabbitmq.demo.common.Constant;
import lddxfs.rabbitmq.demo.common.LocalConnectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 第一个demo 发送端
 */
public class Send {
    private static Logger logger = LoggerFactory.getLogger(Send.class);

    public static void main(String[] argv) throws Exception {
        //ConnectionFactory设置连接的主机 账号和密码
        ConnectionFactory factory = LocalConnectFactory.getConnectionFactory();
        //从ConnectionFactory中获取Connection
        //从Connection获取Channel
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            //使用Channel声明queue
            channel.queueDeclare(Constant.HELLOWORLD_QUEUE_NAME, false, false, false, null);
            String message = "中文123";
            //使用Channel发送消息
            channel.basicPublish("", Constant.HELLOWORLD_QUEUE_NAME, null, message.getBytes("UTF-8"));
            logger.info(" [x] Sent '{}'", message);
        }
        //2018-11-05 07:47:22.718 [main] INFO  lddxfs.rabbitmq.helloworld.Send -  [x] Sent 'Hello World!'

    }
}
