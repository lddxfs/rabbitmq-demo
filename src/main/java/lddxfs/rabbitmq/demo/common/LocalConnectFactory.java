package lddxfs.rabbitmq.demo.common;

import com.rabbitmq.client.ConnectionFactory;

/**
 * Author:lddxfs（lddxfs@qq.com;https://www.cnblogs.com/LDDXFS/）
 * Date:2018/11/5
 */
public class LocalConnectFactory {

    public static ConnectionFactory getConnectionFactory(){
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("127.0.0.1");
        factory.setUsername("lddxfs");
        factory.setPassword("123456");
        factory.setPort(5672);
        return factory;
    }

    /**
     * Author:lddxfs（lddxfs@qq.com;https://www.cnblogs.com/LDDXFS/）
     * Date:2018/11/5
     */
    public static class Constant {
    }
}
