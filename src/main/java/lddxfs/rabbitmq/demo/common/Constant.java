package lddxfs.rabbitmq.demo.common;

/**
 * Author:lddxfs（lddxfs@qq.com;https://www.cnblogs.com/LDDXFS/）
 * Date:2018/11/5
 */
public class Constant {
    public final static String HELLOWORLD_QUEUE_NAME = "hello";
    public static final String TASK_QUEUE_NAME = "task_queue";
    public static final String EXCHANGE_NAME_DIRECT = "direct_logs";
    public static final  String[]  DIRECT_LOG_SEVERITY=new String[]{"debug","info","warn","error"};
    public static final String FANOUT_EXCHANGE_NAME = "logs";
    public static final String TOPIC_EXCHANGE_NAME = "topic_logs";
}
