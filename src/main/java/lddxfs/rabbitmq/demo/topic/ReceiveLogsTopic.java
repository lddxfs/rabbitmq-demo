package lddxfs.rabbitmq.demo.topic;

import com.rabbitmq.client.*;
import lddxfs.rabbitmq.demo.common.Constant;
import lddxfs.rabbitmq.demo.common.LocalConnectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class ReceiveLogsTopic {
    private static Logger logger = LoggerFactory.getLogger(ReceiveLogsTopic.class);

    public static void main(String[] args) throws Exception {
        receive(new String[]{"#"});
        receive(new String[]{"kern.*"});
        receive(new String[]{"*.critical"});
        receive(new String[]{"kern.*", "*.critical"});
    }

    private static void receive(String[] bindingKeys) throws Exception {
        ConnectionFactory factory = LocalConnectFactory.getConnectionFactory();
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare(Constant.TOPIC_EXCHANGE_NAME, BuiltinExchangeType.TOPIC);
        String queueName = channel.queueDeclare().getQueue();
        for (String bindingKey : bindingKeys) {
            channel.queueBind(queueName, Constant.TOPIC_EXCHANGE_NAME, bindingKey);
        }
        logger.info(" [*] Waiting for messages. To exit press CTRL+C");
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                logger.info(" [x] Received '" + envelope.getRoutingKey() + "':'" + message + "'");
            }
        };
        channel.basicConsume(queueName, true, consumer);
    }
}

