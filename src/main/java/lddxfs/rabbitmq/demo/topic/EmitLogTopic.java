package lddxfs.rabbitmq.demo.topic;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import lddxfs.rabbitmq.demo.common.Constant;
import lddxfs.rabbitmq.demo.common.LocalConnectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EmitLogTopic {
    private static Logger logger = LoggerFactory.getLogger(EmitLogTopic.class);

    public static void main(String[] argv) {
//       send("kern.critical","A critical kernel error");
       send("kern.ccc","A critical kernel error");
    }

    public static void send(String routingKey, String message) {
        ConnectionFactory factory = LocalConnectFactory.getConnectionFactory();
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.exchangeDeclare(Constant.TOPIC_EXCHANGE_NAME, BuiltinExchangeType.TOPIC);
            channel.basicPublish(Constant.TOPIC_EXCHANGE_NAME, routingKey, null, message.getBytes("UTF-8"));
            logger.info("发送消息 '{}':'{}'", routingKey, message);
        } catch (Exception e) {
            logger.error("系统异常，接收消息异常", e);
        }
    }

}

