package lddxfs.rabbitmq.demo.direct;

/**
 * direct接收端
 */
public class ReceiveLogsDirect {


    public static void main(String[] argv) throws InterruptedException {
        ReceiveLogsDirectThread threadA = new ReceiveLogsDirectThread("接收端AAA",new String[]{"debug", "info"});
        ReceiveLogsDirectThread threadB = new ReceiveLogsDirectThread("接收端BBB",new String[]{"warn"});
        ReceiveLogsDirectThread threadC = new ReceiveLogsDirectThread("接收端CCC",new String[]{"error"});
        threadA.start();
        threadB.start();
        threadC.start();
        while (true){
            Thread.sleep(Integer.MAX_VALUE);
        }

    }

}

