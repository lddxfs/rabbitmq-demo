package lddxfs.rabbitmq.demo.direct;

import com.rabbitmq.client.*;
import lddxfs.rabbitmq.demo.common.Constant;
import lddxfs.rabbitmq.demo.common.LocalConnectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Author:lddxfs（lddxfs@qq.com;https://www.cnblogs.com/LDDXFS/）
 * Date:2018/11/5
 */
public class ReceiveLogsDirectThread extends Thread {
    private static Logger logger = LoggerFactory.getLogger(ReceiveLogsDirectThread.class);
    private String[] routeingKeys;
    private String receiverName;
    @Override
    public void run() {
        try {
            receiveLog(routeingKeys);
        } catch (Exception e) {
            logger.error("发生异常了", e);
        }
    }

    public ReceiveLogsDirectThread(String receiverName,String[] routeingKeys) {
        this.receiverName = receiverName;
        this.routeingKeys = routeingKeys;
    }

    public void receiveLog(String[] routingKeys) throws Exception {
        ConnectionFactory factory = LocalConnectFactory.getConnectionFactory();
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(Constant.EXCHANGE_NAME_DIRECT, BuiltinExchangeType.DIRECT);
        String queueName = channel.queueDeclare().getQueue();
        for (String routingKey : routingKeys) {
            channel.queueBind(queueName, Constant.EXCHANGE_NAME_DIRECT, routingKey);
        }
        logger.info(" {}正在接收消息...", receiverName);
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                logger.info(" {}接收到消息 '{}':'{}'", receiverName, envelope.getRoutingKey(), message);
            }
        };
        channel.basicConsume(queueName, true, consumer);
    }
}
