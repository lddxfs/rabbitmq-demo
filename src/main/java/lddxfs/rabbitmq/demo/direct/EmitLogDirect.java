package lddxfs.rabbitmq.demo.direct;

import com.rabbitmq.client.*;
import lddxfs.rabbitmq.demo.common.Constant;
import lddxfs.rabbitmq.demo.common.LocalConnectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;
import java.util.UUID;

/**
 * direct发送端
 */
public class EmitLogDirect {
    private static Logger logger = LoggerFactory.getLogger(EmitLogDirect.class);


    public static void main(String[] argv) throws Exception {

        ConnectionFactory factory = LocalConnectFactory.getConnectionFactory();
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.exchangeDeclare(Constant.EXCHANGE_NAME_DIRECT, BuiltinExchangeType.DIRECT);
            String message;
            String routingKey;
            Random random = new Random();
            int i = 0;
            while (i++ < 20) {
                routingKey = Constant.DIRECT_LOG_SEVERITY[random.nextInt(Constant.DIRECT_LOG_SEVERITY.length)];
                message = getMessage();
                // 持久化  MessageProperties.PERSISTENT_TEXT_PLAIN
                channel.basicPublish(Constant.EXCHANGE_NAME_DIRECT, routingKey, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes("UTF-8"));
                logger.info("发送了消息：'{}':'{}'", routingKey, message);
            }
        }
    }


    private static String getMessage() {
        return "消息_".concat(UUID.randomUUID().toString());
    }

}

