package lddxfs.rabbitmq.demo.workqueue;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;
import lddxfs.rabbitmq.demo.common.Constant;
import lddxfs.rabbitmq.demo.common.LocalConnectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

/**
 * 工作队列，创建任务
 */
public class NewTask {
    private static Logger logger = LoggerFactory.getLogger(NewTask.class);

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = LocalConnectFactory.getConnectionFactory();
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(Constant.TASK_QUEUE_NAME, true, false, false, null);
            String message;
            int i = 0;
            while (i++ < 10) {
                message = getMessage("测试消息", Integer.toString(i), UUID.randomUUID().toString());
                channel.basicPublish("", Constant.TASK_QUEUE_NAME,
                        MessageProperties.PERSISTENT_TEXT_PLAIN,
                        message.getBytes("UTF-8"));
                logger.info("发送了消息 '{}'", message);
            }

        }
    }

    private static String getMessage(String... strings) {
        return String.join(" ", strings);
    }

}
