package lddxfs.rabbitmq.demo.workqueue;

import com.rabbitmq.client.*;
import lddxfs.rabbitmq.demo.common.Constant;
import lddxfs.rabbitmq.demo.common.LocalConnectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * 工作队列，执行任务
 * 为了达到效果，运行多次main方法（开启多个worker），则每个Worker只会接收到部分消息
 */
public class Worker {
    private static Logger logger = LoggerFactory.getLogger(Worker.class);

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = LocalConnectFactory.getConnectionFactory();
        final Connection connection = factory.newConnection();
        final Channel channel = connection.createChannel();

        channel.queueDeclare(Constant.TASK_QUEUE_NAME, true, false, false, null);
        logger.info(" [*] Waiting for messages. To exit press CTRL+C");

        channel.basicQos(1);

        final Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");

                logger.info(" [x] Received '{}'",message);
                try {
                    doWork(message);
                } finally {
                    logger.info(" [x] Done");
                    channel.basicAck(envelope.getDeliveryTag(), false);
                }
            }
        };
        channel.basicConsume(Constant.TASK_QUEUE_NAME, false, consumer);
    }

    /**
     * 运行结果1
     * 2018-11-05 09:06:08.608 [pool-1-thread-6] INFO  lddxfs.rabbitmq.workqueue.Worker -  [x] Received '测试消息 1 3e5a6bbc-15ce-4e5c-a743-c0cf8c7b89a2'
     * 2018-11-05 09:06:08.608 [pool-1-thread-6] INFO  lddxfs.rabbitmq.workqueue.Worker -  [x] Done
     * 2018-11-05 09:06:08.612 [pool-1-thread-7] INFO  lddxfs.rabbitmq.workqueue.Worker -  [x] Received '测试消息 3 0ce37308-8ded-4204-afa0-8c439e176d72'
     * 2018-11-05 09:06:08.612 [pool-1-thread-7] INFO  lddxfs.rabbitmq.workqueue.Worker -  [x] Done
     * 2018-11-05 09:06:08.613 [pool-1-thread-8] INFO  lddxfs.rabbitmq.workqueue.Worker -  [x] Received '测试消息 5 5493d838-6618-4c06-abc3-5a226c30a2a8'
     * 2018-11-05 09:06:08.613 [pool-1-thread-8] INFO  lddxfs.rabbitmq.workqueue.Worker -  [x] Done
     * 2018-11-05 09:06:08.613 [pool-1-thread-9] INFO  lddxfs.rabbitmq.workqueue.Worker -  [x] Received '测试消息 7 b49eccca-6ab7-47f4-ad03-38f9656bf990'
     * 2018-11-05 09:06:08.613 [pool-1-thread-9] INFO  lddxfs.rabbitmq.workqueue.Worker -  [x] Done
     * 2018-11-05 09:06:08.614 [pool-1-thread-10] INFO  lddxfs.rabbitmq.workqueue.Worker -  [x] Received '测试消息 9 692b65a4-2053-43d7-be8a-2276442799cd'
     * 2018-11-05 09:06:08.614 [pool-1-thread-10] INFO  lddxfs.rabbitmq.workqueue.Worker -  [x] Done
     * 运行结果2
     * 2018-11-05 09:06:08.612 [pool-1-thread-23] INFO  lddxfs.rabbitmq.workqueue.Worker -  [x] Received '测试消息 2 21788f3b-669d-4968-a4d3-58d19acf0522'
     * 2018-11-05 09:06:08.612 [pool-1-thread-23] INFO  lddxfs.rabbitmq.workqueue.Worker -  [x] Done
     * 2018-11-05 09:06:08.613 [pool-1-thread-24] INFO  lddxfs.rabbitmq.workqueue.Worker -  [x] Received '测试消息 4 63f85efe-6abb-41ff-9f42-8bea333c01ab'
     * 2018-11-05 09:06:08.613 [pool-1-thread-24] INFO  lddxfs.rabbitmq.workqueue.Worker -  [x] Done
     * 2018-11-05 09:06:08.613 [pool-1-thread-25] INFO  lddxfs.rabbitmq.workqueue.Worker -  [x] Received '测试消息 6 307447b0-40b3-4d9b-ad99-1158a1ff4fae'
     * 2018-11-05 09:06:08.613 [pool-1-thread-25] INFO  lddxfs.rabbitmq.workqueue.Worker -  [x] Done
     * 2018-11-05 09:06:08.613 [pool-1-thread-26] INFO  lddxfs.rabbitmq.workqueue.Worker -  [x] Received '测试消息 8 45943378-513b-4394-a4d8-43d31596fc92'
     * 2018-11-05 09:06:08.613 [pool-1-thread-26] INFO  lddxfs.rabbitmq.workqueue.Worker -  [x] Done
     * 2018-11-05 09:06:08.614 [pool-1-thread-3] INFO  lddxfs.rabbitmq.workqueue.Worker -  [x] Received '测试消息 10 b0b6a1db-5f4d-4a57-a57d-bbf8e6e01dca'
     * 2018-11-05 09:06:08.614 [pool-1-thread-3] INFO  lddxfs.rabbitmq.workqueue.Worker -  [x] Done
     */

    private static void doWork(String task) {
        for (char ch : task.toCharArray()) {
            if (ch == '.') {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException _ignored) {
                    Thread.currentThread().interrupt();
                }
            }
        }
    }
}

